/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package flappy;

/**
 *
 * @author Luis
 */

//import java.awt.Image;

public class Tubo extends Objeto {
    private boolean flag;
    public Tubo(int posX, int posY, Animacion animacion, boolean flag) {
        
        super(posX, posY, animacion);
        flag=false;
        
    }
    
    public boolean getFlag() {
        
        return flag;
        
    }
    
    public void setFlag(boolean st) {
        
        flag = st;
        
    }
    
}