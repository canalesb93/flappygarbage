/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package flappy;

/**
 * v.1.0
 * @author Ricardo
 * A01191118 y A01191463
 */

import javax.swing.JFrame;
//import java.applet.AudioClip;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
//import java.net.URL;
//import java.util.Iterator;
//import java.util.Random;
import java.io.File;
//import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;


public class Flappy extends JFrame implements Runnable, KeyListener, MouseListener {

    private Image dbImage;
    private Graphics dbg;
    private final int gravedad = 1;
    private int velocidadVertical, velocidadTubos;
    private Bola bola, boton;
    private int score, highscore, random, high, low;
    private int vidas;
    private boolean activo, guardar, cargar, silencio;
    private int state;
    private int distancia=300;
    private long tiempoActual;
    private long tiempoBola=0;
    private LinkedList<Tubo> tubos;
    //Sonidos
    private SoundClip bomb, punch, jump, hop, laser;
     //Animaciones
    private Animacion pelotaAnim, pelotaTp, tuboArAnim, tuboAbAnim, botonAnim;
    private boolean swap=false;
    private boolean teleported;
    Image background1 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("background1.jpg"));
    Image background2 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("background2.jpg"));
    Image endMessage = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("endMessage.png"));
    Image startMessage = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("startMessage.png"));
    
    public Flappy(){
        init();
        start();
    }
    
    public void init() {
        
        setSize(1000, 700);
        addKeyListener(this);
        addMouseListener(this);
        
        
        //SoundClips
        bomb = new SoundClip("Explosion.wav");
        laser = new SoundClip("laser.wav");
        punch = new SoundClip("punch.wav");
        jump = new SoundClip("jump.wav");
        hop = new SoundClip("hop.wav");
        //Animaciones
	Image bola1 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("bola4.png"));
        Image bola2 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("bola2.png"));
        Image bola3 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("bola3.png"));
        Image bolaTp1 = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("bolaTp1.png"));
        Image tuboAr = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("tuboArriba.png"));
        Image tuboAb = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("tuboAbajo.png"));
        Image playMessage = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("playMessage.png"));
	

        
	high=370;
        low=70;
	pelotaAnim = new Animacion();
        pelotaTp = new Animacion();
        tuboArAnim = new Animacion();
        tuboAbAnim = new Animacion();
        botonAnim = new Animacion();
        
	pelotaAnim.sumaCuadro(bola1, 100);
        pelotaAnim.sumaCuadro(bola2, 100);
        pelotaAnim.sumaCuadro(bola3, 100);
        pelotaTp.sumaCuadro(bolaTp1, 100);
//        pelotaAnim.sumaCuadro(bola4, 100);
//        pelotaAnim.sumaCuadro(bola5, 100);
//        pelotaAnim.sumaCuadro(bola6, 100);

        tuboArAnim.sumaCuadro(tuboAr, 100);
        tuboAbAnim.sumaCuadro(tuboAb, 100);
        botonAnim.sumaCuadro(playMessage, 100);

        

        velocidadVertical = 0;
        velocidadTubos = -6;
        tubos = new LinkedList();
        


        vidas = 1;
        //contador = 3;
        activo = false;
        teleported=false;
        guardar = cargar = silencio = false;
        state = 0;
        
        int xInicial = getWidth();
        bola = new Bola(getWidth()/4, getHeight()/2, pelotaAnim);
        boton = new Bola(getWidth()/2-playMessage.getWidth(this)/2, (getHeight()/8)*6 -25, botonAnim);
        for(int i = 0; i < 3; i++){
            xInicial+=333;
            random = (int)((Math.random() * high +low));
            tubos.add(new Tubo(xInicial,random-tuboArAnim.getImagen().getIconHeight(),tuboArAnim,false));
            tubos.add(new Tubo(xInicial,random+distancia,tuboAbAnim,false));
        }
    }
    public void start() {
        
        Thread th = new Thread(this);
        th.start();
        
    }
    
    public void run() {
        tiempoActual = System.currentTimeMillis();
        while(true){
        
            actualiza();
            checaColision();
            repaint();
            try{
                Thread.sleep(20);
            } 
            catch(InterruptedException ex){
                System.out.println("Error en " + ex.toString());
            }
        }
    }
    
    void actualiza(){
        //Determina el tiempo que ha transcurrido desde que el Applet inicio su ejecución
         if(guardar){
             guardar = false;
             try {
                 grabaArchivo();
             } catch(IOException e) {
                 System.out.println("Error en guardar");
             }
         }
         if(cargar){
             cargar=false;
             try {
                leeArchivo();
             } catch(IOException e) {
                 System.out.println("Error en cargar");
             }
         }
         long tiempoTranscurrido = System.currentTimeMillis() - tiempoActual;
            
         //Guarda el tiempo actual
       	 tiempoActual += tiempoTranscurrido;

         //Actualiza la animación en base al tiempo transcurrido
         
        if(state == 0) {
            if(activo){
                if(score==10){
                    velocidadTubos=-7;
                    distancia=250;
                }                    
                if(score==20){
                    velocidadTubos=-8;
                    distancia=210;
                }
                if(teleported){
                    if(tiempoActual-tiempoBola>100){
                        bola.setImageIcon(pelotaAnim);
                        bola.setPosY(bola.getPosY()+160);
                        teleported=false;
                    }
                }
                bola.setPosY(bola.getPosY() + velocidadVertical);
                pelotaAnim.actualiza(tiempoTranscurrido);

                velocidadVertical += gravedad;
                random = (int)((Math.random() * high+low));
                boolean papa = false;
                
                for(Tubo losTubos : tubos){
                    losTubos.setPosX(losTubos.getPosX()+velocidadTubos);
                    if(losTubos.getPosX()<bola.getPosX()&&!losTubos.getFlag()){
                        score++;
                        if(!silencio)
                            jump.play();
                        losTubos.setFlag(true);
                    }
                    if(losTubos.getPosX()+losTubos.getAncho()<10){
                        losTubos.setPosX(getWidth());
                        if(!papa){
                            losTubos.setPosY(random-tuboArAnim.getImagen().getIconHeight());
                            papa = true;
                            losTubos.setFlag(false);
                        }
                        else{
                            losTubos.setPosY(random+distancia);
                            papa = false;
                            losTubos.setFlag(false);
                        }
                        
                        
                    }
                    
                }
            }

//            if(up && canasta.getPosX() > getWidth()/2){
//                
//            }
//            else if(down && canasta.getPosX()+canasta.getAncho() < getWidth()){
//                
//            }
//            else{
//                canastaAnim.actualiza(tiempoTranscurrido);
//                canasta.setImageIcon(canastaAnim);
//            }
//        }
        
        }
    }
    
           public void leeArchivo() throws IOException {
                                                          
                BufferedReader fileIn;
                try {
                        fileIn = new BufferedReader(new FileReader("flappy"));
                } catch (FileNotFoundException e){
                        File puntos = new File("flappy");
                        PrintWriter fileOut = new PrintWriter(puntos);
                        fileOut.println("100,demo");
                        fileOut.close();
                        fileIn = new BufferedReader(new FileReader("flappy"));
                }
                String dato = fileIn.readLine();
                highscore = (Integer.parseInt(dato));
                      
                fileIn.close();
        }
    
    public void grabaArchivo() throws IOException {
                PrintWriter fileOut = new PrintWriter(new FileWriter("flappy"));
                fileOut.println(String.valueOf(score));

                fileOut.close();
        }
    
    void checaColision() {
        

        if(bola.getPosY() < 28){
            bola.setPosY(28);          
        }
        for(Tubo losTubos : tubos){
            if(bola.intersecta(losTubos)||bola.getPosY() > getHeight()){
                if(!silencio&&vidas!=0)
                    bomb.play(); 
                vidas=0;
                
                    
            }
        }
        
//        if(bola.intersecta(canasta)){
//            
//            bola.setPosX(100);
//            bola.setPosY(400);
//            velocidadVertical = (int)(-1 * (Math.random() * 15 + 10));
//            velocidadHorizontal = (int)((Math.random() * 6) + 10);
//            score +=2;
//            activo = false;
//            if(!silencio)
//                punch.play();
//            
//        }
        
    }
    
    public void keyPressed(KeyEvent e) {
        
        if(e.getKeyCode() == KeyEvent.VK_UP) {
            if(vidas!=0){
                activo=true;
                velocidadVertical=-15;
                vidas=1;
            }
            else{
                restart();
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            if(activo&&vidas==1){
                if(!silencio)
                    laser.play();
                bola.setImageIcon(pelotaTp);
                teleported=true;
                tiempoBola=tiempoActual;
                velocidadVertical=0;
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_P) {
            if(state == 1){
                state = 0;
            } else {
                state = 1;
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_I) {
            if(state == 2){
                state = 0;
            } else {
                state = 2;
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_G) {
            guardar=true;
        }
        if(e.getKeyCode() == KeyEvent.VK_C) {
            cargar=true;
        }
        if(e.getKeyCode() == KeyEvent.VK_S) {
            silencio=!silencio;
        }
        if(e.getKeyCode() == KeyEvent.VK_SPACE) {            
            restart();
        }
    }
    
    public void keyReleased(KeyEvent e) {
        
        if(e.getKeyCode() == KeyEvent.VK_UP) {
            
        }
        if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            
        }
        
    }
    
    public void restart(){
        swap=!swap;
        velocidadTubos=-6;
        state=0;
        activo=false;
        vidas=1;
        score=0;
        distancia=300;
        int xInicial = getWidth();
        boolean papa = false;
        for(Tubo losTubos : tubos){
            if(!papa){
                random = (int)((Math.random() * high +low));
                losTubos.setPosY(random-tuboArAnim.getImagen().getIconHeight());
                papa = true;
                losTubos.setPosX(xInicial);
                losTubos.setFlag(false);
            }else{

                losTubos.setPosY(random+distancia);
                papa = false;
                losTubos.setPosX(xInicial);
                xInicial+=333;
                losTubos.setFlag(false);
            }
        }
        bola.setPosX(getWidth()/4);
        bola.setPosY(getHeight()/2); 
    }
    
    public void mousePressed(MouseEvent e) {
        
        if(boton.getPerimetro().contains(e.getX(), e.getY())&&vidas==0) {
            restart();
        }
        
    }
    
    public void paint(Graphics g) {
        
        if(dbImage == null) {
            dbImage = createImage(this.getSize().width, this.getSize().height);
            dbg = dbImage.getGraphics ();
        }

        dbg.setColor(getBackground ());
	dbg.fillRect(0, 0, this.getSize().width, this.getSize().height);

	dbg.setColor(getForeground());
	paint1(dbg);

	g.drawImage (dbImage, 0, 0, this);
        
    }
        
    public void paint1 (Graphics g){
        if(swap)
            g.drawImage(background1, 0, 0, null);
        else
            g.drawImage(background2, 0, 0, null);
        if(bola != null){
            if(activo==false&&vidas==1){
                g.drawImage(startMessage, getWidth()/2-startMessage.getWidth(this)/2, getHeight()/2-startMessage.getHeight(this), this);
            }
            g.drawImage(bola.getImagenI(), bola.getPosX(), bola.getPosY(), this);
            for(Tubo losTubos : tubos){
                g.drawImage(losTubos.getImagenI(), losTubos.getPosX(), losTubos.getPosY(), this);    
            }
                    
            g.setColor(Color.white);
                g.setFont(new Font("arial", Font.BOLD, 60));
                g.drawString(String.valueOf(score/2),getWidth()/2 - 20, getHeight()/4);
                
            if(vidas==0){
                try {
                    leeArchivo();
                } catch(IOException e) {
                    System.out.println("Error en cargar");
                }
                if(score>highscore){
                    try {
                        grabaArchivo();
                    } catch(IOException e) {
                        System.out.println("Error en guardar");
                    }
                }
                g.drawImage(endMessage, getWidth()/2-endMessage.getWidth(this)/2, getHeight()/2-endMessage.getHeight(this)/2, this);
                g.setColor(Color.red);
                g.setFont(new Font("arial", Font.BOLD, 60));
                g.drawString("Game Over", getWidth()/2 - 150, getHeight()/2-60);    
                g.setColor(Color.white);
                g.setFont(new Font("arial", Font.BOLD, 40));
                g.drawString("Score: " + score/2,getWidth()/2 - 200, getHeight()/2);
                g.drawString("HighScore: " + highscore/2,getWidth()/2 - 200, getHeight()/2+60); 
                g.drawImage(boton.getImagenI(), boton.getPosX(), boton.getPosY(), this);
                
                state = 1;
            }
            else if(state == 1) {
                g.setFont(new Font("arial", Font.BOLD, 60));
                g.drawString("PAUSA", getWidth()/2 - 100, getHeight()/2);
            }
            if(state == 2) {
                g.setColor(Color.green);
                g.fillRect(100, 100, getWidth() - 200, getHeight() - 200);
                g.setColor(Color.black);
                g.setFont(new Font("arial", Font.BOLD, 50));
                g.drawString("INSTRUCCIONES", getWidth()/2 - 210, 200);
                g.setFont(new Font("arial", Font.BOLD, 20));
                g.drawString("Objetivo: atrapar la pelota de basketball para hacer puntos.", getWidth()/2 - 300, 220);
                g.drawString("Presionar flechas(izquierda y derecha) para moverse.", getWidth()/2 - 300, 240);
                g.drawString("Presionar 'i' para instruciones.", getWidth()/2 - 300, 260);
                g.drawString("Presionar 'p' para pausar.", getWidth()/2 - 300, 280);
                g.drawString("Presionar 'g' para guardar el juego.", getWidth()/2 - 300, 300);
                g.drawString("Presionar 'c' para cargar el juego", getWidth()/2 - 300, 320);
                g.drawString("Presionar 's; para silenciar los sonidos.", getWidth()/2 - 300, 340);
                g.drawString("Presionar 'r' para reinicar(experimental) el juego.", getWidth()/2 - 300, 360);
                g.drawString("Tienes 5 vidas y perderas 1 vida cada tercera vez que", getWidth()/2 - 300, 390);
                g.drawString("la pelota toque el piso. Suerte!", getWidth()/2 - 300, 410);
            }
            
        } else {
            
            g.setColor(Color.blue);
            g.drawString("No se cargo la imagen...", 20, 20);
            
        }
        
    }
    
    public static void main(String[] args) {
        
        Flappy basket = new Flappy();
        basket.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        basket.setVisible(true);
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
       
    }

    @Override
    public void mouseClicked(MouseEvent me) {
       
    }

    @Override
    public void mouseReleased(MouseEvent me) {
       
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }

    
}
